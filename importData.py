import matplotlib as mpl
mpl.use('Qt5Agg')

import csv
import numpy as np
from pathlib import Path
import re
from shutil import copyfile

def reverse_zip(*data_list):
    reverse = [data[::-1] for data in data_list]
    return zip(*reverse)

def importScans(path, data_type='Scan', theta=None, intensity=None):

    if theta is None:
        theta = []
        intensity = []

    with open(path, encoding='unicode_escape') as input_file:
        header = input_file.readline() # could seek back to zero position here

        if 'Id:' in ''.join(header):
            delim = ' '
            reader = csv.reader(input_file,delimiter=delim)

            for row in reader:
                theta.append(float(row[0]))
                intensity.append(float(row[1]))

        elif '.' in header[0:5] and '\t' in header:
            delim = '\t'
            reader = csv.reader(input_file,delimiter=delim)

            header = header.replace('\n',delim).split(delim)
            theta.append(float(header[0]))
            intensity.append(float(header[1]))

            for row in reader:
                theta.append(float(row[0]))
                intensity.append(float(row[1]))

        elif '.' in header[0:4] and ' ' in header:
            delim = ' '
            reader = csv.reader(input_file,delimiter=delim)

            header = header.replace('\n',delim).split(delim)
            theta.append(float(header[0]))
            intensity.append(float(header[1]))

            for row in reader:
                theta.append(float(row[0]))
                intensity.append(float(row[1]))

        elif ',' in header[0:4]:
            delim = ' '
            reader = csv.reader(input_file,delimiter=delim)

            header = header.replace('\n',delim).replace(',','.').split(delim)
            theta.append(float(header[0]))
            intensity.append(float(header[1]))

            for row in reader:
                theta.append(float(row[0].replace(',','.')))
                intensity.append(float(row[1].replace(',','.')))

        elif data_type == 'Ref':
            header2 = input_file.readline()
            # add check here for data dspace or theta
            if '\t' in header2:
                delim = '\t'
                reader = csv.reader(input_file,delimiter=delim)
                
                header2 = header2.replace('\n',delim).split(delim)
                theta.append(1/float(header2[0]))
                intensity.append(float(header2[1]))

                for row in reader:
                    theta.append(1/float(row[0]))
                    intensity.append(float(row[1]))


            if ' ' in header2:
                delim = ' '
                rows = input_file.readlines()

                header2 = re.split(delim+'+',header2.replace('\n',delim))
                theta.append(1/float(header2[0]))
                intensity.append(float(header2[1]))

                data = [re.split(' +',row.replace('\n',delim)) for row in rows]

                for row in data:
                    theta.append(1/float(row[0]))
                    intensity.append(float(row[1]))  

    return np.array(theta), np.array(intensity)


def dspace_conv(theta, source, target='dspa'):
    sources = ['Cu', 'Cr', 'Fe', 'Co', 'Mo'];
    wavelengths = [1.5406, 2.2897, 1.93604, 1.78897, 0.7093];
    source_dict = {x:y for x,y in zip(sources, wavelengths)}

    wav = source_dict[source]

    if target == 'dspa':
        dspa = 1/(wav/(2*np.sin(theta/2*np.pi/180)))
        return dspa
    else:
        deg = np.arcsin(wav/2*theta)*2*180/np.pi
        return deg



def plotting(self, file_path):
    source = file_path / 'Styles' / 'thesis.mplstyle'
    dest = Path(mpl.get_configdir()) / 'stylelib' / 'thesis.mplstyle'

    if dest.parent.exists() == False:
        dest.parent.mkdir()

    copyfile(source,dest)
    import matplotlib.pyplot as plt
    
    self.update_table_combobox()
    if self.plot_num > 0:
        man = plt.get_current_fig_manager().window
        x, y, h, w = man.geometry().getRect()

    plt.close()
    plt.ion()
    
    tbl = self.table_data
    hex_colours = tbl[0]
    cols = list([tuple(int(col.lstrip('#')[i:i+2], 16)/255 
                            for i in (0, 2 ,4)) for col in hex_colours])
    names = list(tbl[1])
    scan_paths = list(tbl[2])
    data_types = list(tbl[3])
    source = list(tbl[4])
    y_spacing = list([float(n) for n in tbl[5]])
    y_scale = list([float(n) for n in tbl[6]])
    x_shift = list([float(n) for n in tbl[7]])
    markers = list(tbl[8])

    cols.reverse()
    names.reverse()
    scan_paths.reverse()
    data_types.reverse()
    y_spacing.reverse()
    y_scale.reverse()
    x_shift.reverse()
    source.reverse()
    markers.reverse()
    
    # Dictionary is better here
    normalise = self.normalise.isChecked()
    legend = self.legend.isChecked()
    box = self.box.isChecked()
    dspace = self.dspace.isChecked()
    latex = self.latex.isChecked()
    raman = self.raman.isChecked()
    series_label = self.series_label.isChecked()
    bold_label = self.bold_label.isChecked()
    bold_number = self.bold_number.isChecked()
    label_colour = self.label_colour.isChecked()

    x_label = self.x_label.text()
    y_label = self.y_label.text()
    x_min = float(self.x_min.text())
    x_max = float(self.x_max.text())
    x_inc = float(self.x_inc.text())
    scan_width = float(self.scan_width.text())
    ref_width = float(self.ref_width.text())
    font_size = float(self.font_size.text())
    fig_height = float(self.fig_height.text())
    fig_width = float(self.fig_width.text())
    label_x_pos = float(self.label_x_pos.text())
    label_y_pos = float(self.label_y_pos.text())
    label_font_size = float(self.label_font_size.text())

    sources = ['Cu', 'Cr', 'Fe', 'Co', 'Mo'];
    wavelengths = [1.5406, 2.2897, 1.93604, 1.78897, 0.7093];
    source_dict = {x:y for x,y in zip(sources, wavelengths)}
    bold_dict = {1:'bold', 0:'normal'}
    unit_dict = {'Centimetres' : 2.54,
                 'Inches' : 1,
                 'Points' : 72,
                 'Pixels' : 100}
    marker_options = ['.', ',', 'o', 'v', 
        '^', '<', '>', '1', '2', '3', '4',
        's', 'p', '*', 'h', 'H', '+', 'x',
        'D', 'd', '|', '_',] 

    max_intensity = []
    all_theta = []
    all_intensity = []
    intensity_stacked = []
    all_bases = [[] for scan in scan_paths]
    lines = []

    ####################################################################
    ####### Import
    ####################################################################

    for n, (path, element) in enumerate(zip(scan_paths,source)):

        data_type = data_types[n]
        theta, intensity = importScans(path, data_type=data_type)

        if normalise == 1:
            intensity = intensity/np.max(intensity)

        if dspace == 0 and data_type == 'Ref':
            theta = dspace_conv(theta, element, target='theta')
        elif dspace == 1 and data_type == 'Scan':
            theta = dspace_conv(theta, element)

        max_intensity.append(np.max(intensity))
        all_theta.append(theta)
        all_intensity.append(intensity)

    max_raw = np.max(max_intensity)

    ####################################################################
    ####### Style and format
    ####################################################################

    plt.style.use('thesis')
    mpl.rcParams['font.weight'] = bold_dict[bold_number]
    mpl.rcParams['axes.labelweight'] = bold_dict[bold_label]
    mpl.rcParams['font.sans-serif'] = self.font.currentText()
    mpl.rcParams['xtick.direction'] = self.defaults['d_tick_dir']
    mpl.rcParams['ytick.direction'] = self.defaults['d_tick_dir']

    hfont = {'size' : font_size, 
            'weight' : 'normal', 
            'family' : self.font.currentText()}

    lfont = {'size' : label_font_size, 
            'weight' : 'normal', 
            'family' : self.font.currentText(),
            'color' : 'k'}

    dpi = mpl.rcParams['figure.dpi']
    unit_dict['Pixels'] = dpi
    fig_width_inch = fig_width/unit_dict[self.unit_combo.currentText()]
    fig_height_inch = fig_height/unit_dict[self.unit_combo.currentText()]
    
    fig, ax = plt.subplots(1,1,figsize=(fig_width_inch,fig_height_inch))

    ####################################################################
    ####### Process data
    ####################################################################
    # Format y-spacing if latex are being used
    if latex:
        y_spacing = []
        order = data_types[::-1]
        if order[0] == 'Ref':
            print('First scan must be a scan')
        else:
            next_row = order[1]
            for n, o in enumerate(order):
                if n < len(order)-1:
                    next_row = order[n+1]
                    if next_row =='Scan':
                        y_spacing.append(0.1)
                    else:
                        y_spacing.append(-1)
        y_spacing.append(0.1)
        y_spacing = y_spacing[::-1]

    # Stacking of data
    for n, (theta, intensity, data_type) in enumerate((
            zip(all_theta, all_intensity, data_types))):

        if data_type == 'Scan':
            if n > 0:
                intensity_temp = (intensity * y_scale[n] + 
                                  np.max(intensity_stacked[n-1])
                                  + y_spacing[n] * max_raw)
            else:
                intensity_temp = (intensity * y_scale[n] 
                                  + y_spacing[n] * max_raw)
            
            intensity_stacked.append(intensity_temp)

        else:
            intensity_temp = intensity/np.max(intensity) * max_raw * y_scale[n]
            intensity_stacked.append(intensity_temp)
            base = -1

            if n > 0:
                base = np.max(intensity_stacked[n-1]) + y_spacing[n] * max_raw
                intensity_temp = base + intensity_temp
                intensity_stacked[n] = intensity_temp

            all_bases[n] = base

    ####################################################################
    ####### Plot
    ####################################################################
    theta_max = np.max([np.max(i) for i in all_theta])
    for theta, intensity, base, data_type, col, marker in reverse_zip(
                all_theta, intensity_stacked, all_bases, 
                data_types, cols, markers):
        if data_type == 'Scan':
            line, = plt.plot(theta, intensity, 
                            color=col, linewidth=scan_width)
            current_intensity = intensity
            current_theta = theta
            lines.append(line)
        elif latex:
            if marker not in marker_options:
                marker = 'o'
                
            for n, (deg, intens) in enumerate(zip(theta,intensity)):
                check = (current_theta >= deg*0.99) & (current_theta <= deg*1.01)
                if any(check):
                    intens = current_intensity[np.where(check)].max() + max_raw*0.2
                    line, = ax.plot(deg, intens, marker=marker, 
                                markerfacecolor='none', markersize=5,
                                color=col, linewidth=ref_width)
        else:
            for n, (deg, intens) in enumerate(zip(theta,intensity)):
                if n == 0:
                    line, = ax.plot([deg, deg], [base, intens], 
                            color=col, linewidth=ref_width, 
                            solid_capstyle='butt')
                    lines.append(line)
                else:
                    ax.plot([deg, deg], [base, intens], 
                            color=col, linewidth=ref_width, 
                            solid_capstyle='butt')
                
            ax.plot([0, theta_max], [base, base], color='k')

    ####################################################################
    ####### Format plot
    ####################################################################

    ax.set_xticks(np.arange(x_min, x_max+0.01, x_inc))
    y_max = np.max([np.max(i) for i 
            in intensity_stacked])*float(self.defaults["d_top_space"])
    plt.xlim((x_min,x_max))

    if dspace == 1:
        raman = 0 # dspace takes preference over raman
        if any(x > 3 for x in [x_min, x_max, x_inc, label_x_pos]):
            element = max(set(source), key=source.count)
            x_min, x_max, x_inc, label_x_pos = [
                            dspace_conv(val,element).tolist() 
                            if val > 3 else val 
                            for val in [x_min, x_max, x_inc, label_x_pos]]
            ax.relim()
            ax.autoscale()
            plt.xlim((x_min,x_max))
            ax.get_xaxis().set_major_locator(mpl.ticker.AutoLocator())

            print(('One x-axis setting appears to be in 2θ units,' 
                    f'converting to 1/d using {element} as a source'))

        if x_label == self.defaults["d_x_label"] and ('°' in x_label or 'θ' in x_label):
            x_label = '1/d (1/Å)'
            print(('x-axis label appears to be incorrectly labelled, '
                    'changing to "1/d (1/Å)"'))
            
    if series_label == 1:
        legend = 0
        inv = ax.transData.inverted()
        r = fig.canvas.get_renderer()
        texts = []

        for name, col in zip(names, cols):
            if label_colour:
                lfont['color'] = col

            texts.append(plt.text(label_x_pos, 1, name, **lfont))

        for text, intensity, theta, base in (
                        zip(texts, intensity_stacked, all_theta, all_bases)):
            extents = inv.transform(text.get_window_extent(renderer=r))
            if raman:
                x_extent = 2*label_x_pos - extents[1][0]
                check = (theta <= label_x_pos) & (theta >=x_extent)
            else:
                x_extent = extents[1][0]
                check = (theta >= label_x_pos) & (theta <=x_extent)
           
            if any(check) == True:
                max_int = np.max(intensity[np.where(check)])
            else:
                max_int = base
                if base == -1:
                    max_int = 0

            text.set_y(max_int + label_y_pos * max_raw)
            
    if raman == 1:
        if x_max < 200:
            print(('Maximum appears to be low for a Raman scan, changing'
                    ' to bounds to automatic limits'))
            ax.relim()
            ax.autoscale()
            x_min, x_max = plt.xlim()
            plt.xlim((x_max,x_min))
            ax.get_xaxis().set_major_locator(mpl.ticker.AutoLocator())
        else:
            plt.xlim((x_max,x_min))

        if x_label == self.defaults["d_x_label"] and 'raman' not in x_label.lower():
            x_label = 'Raman Shift ($cm^{-1}$)'

    plt.ylim((0, y_max))
    ax.set_yticklabels([])
    plt.xlabel(x_label,fontsize=font_size)
    plt.ylabel(y_label,fontsize=font_size)
    plt.xticks(fontsize=font_size)

    ax.spines['right'].set_visible(box)
    ax.spines['top'].set_visible(box)

    if legend == 1 and series_label!=1:
        self.lines = lines
        self.names = names
        self.lgd = ax.legend(lines, names,
                        loc='center left', bbox_to_anchor=(1,0.5))
    else:
        self.lgd = 1
        plt.tight_layout()

    plt.show()

    if self.plot_num > 0:
        man = plt.get_current_fig_manager().window
        oldx, oldy, h, w = man.geometry().getRect()
        man.setGeometry(x, y, h, w)

    self.fig = fig
    self.plot_num = self.plot_num + 1


