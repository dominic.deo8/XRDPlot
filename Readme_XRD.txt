XRD Plotting Tool

1.	Extract the zip file.

2.	The folder "Scans", under the "dist" folder, is where you you should copy
 
	the scan data, which has been converted from a .raw file to an .xy file.

	To convert the raw file to xy you can export on the XRD computer in 
	the lab, or you can download "PowDLL" converter and convert to .xy 	
	on your own computer

3.	The folder "Refs", under the "dist" folder, is where you should copy
 
	your reference data, which must be in the ".txt" file format.

	You can get the d spacing and Intensity data from the XRD Computer, and
	copy these two columns to excel. Save the excel as a .txt file.

4.	To run the tool, you need to open GUIMainWindow.exe under "dist" in the
 
	"XRD Plot" folder.

5.	A user interface will open once you click run. It should be
	straightforward from there on. You can fiddle around with each button
	to see what they do.


	The fields in the table with the scan names that are editable.

	
	File Name: 	changes the name of the scan displayed in the legend or
				label
	
		Source: 	a drop down list of sources for conversion to "1/d".
	
		Y Spacing:	fractional offset between plotted scans on the y axis
	
	Y Scale:	fractional scale factor for the scan intensity
	
	X shift:	shift in the 2Theta or 1/d value

6.	The only other file you may need to touch is the "defaults.cfg" file.
 
	This file contains the default parameters to be loaded into the Interface
 
	on startup. You can change any of there parameters once you have a
 
	preferred layout of the plots you would like to generate. The format of

	this file should always remain the same i.e. "Property" : "value", so

	when making changes to this file it only necessary to change the value

	inside the quotation marks.


	For check boxes, a value of 1 means checked, 0 means unchecked.

	You can load
	a custom set of defaults in two ways, firstly by using the load	defaults button
	under tools in the menu bar, which then opens a file browser a custom defaults.cfg
	file in the correct format. You can use the save defaults button to generate a
	custom defaults file. "defaults.cfg" is always
	loaded automatically, so the 
	"defaults.cfg" file can be overwritten with the
	custom file if you do not want 
	to load the custom file manually each time.

Other Notes:

1.	The default export format is SVG. This is a vector graphics format which is
	compatible with the latest version of Microsoft Office and will result in the
	highest quality images. You can change the format using any valid image extension.

2.	If you want to export your plot, don't forget to change the title on top, as
	exporting will overwrite the any previous plot named untitled that you have
	plotted before.