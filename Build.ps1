﻿# Comment

cd 'C:\Users\domin\Google Drive (dlvdom001@myuct.ac.za)\Python\XRD\Offline XRD\For Compiling'

pyinstaller --distpath ./App/dist --workpath ./App/build .\GUIMainWindow.spec

Copy-Item -Path .\Refs -Destination .\App\dist -Force -Recurse
Copy-Item -Path .\Scans -Destination .\App\dist -Force -Recurse
Copy-Item -Path ".\Plot Data" -Destination .\App\dist -Force -Recurse
Copy-Item -Path ".\Raman Scans" -Destination .\App\dist -Force -Recurse
Copy-Item -Path .\Scans -Destination .\App\dist -Force -Recurse
Copy-Item -Path .\defaults.cfg -Destination .\App\dist -Force -Recurse
Copy-Item -Path .\Styles -Destination .\App\dist -Force -Recurse
Copy-Item -Path .\Readme_XRD.txt -Destination .\App\dist -Force -Recurse
Copy-Item -Path .\CHANGELOG -Destination .\App\dist -Force -Recurse

$source = 'C:\Users\domin\Google Drive (dlvdom001@myuct.ac.za)\Python\XRD\Offline XRD\For Compiling\App'
$destination = "C:\Users\domin\OneDrive - University of Cape Town\XRD Plot\XRD Plot.zip"
If(Test-path $destination) {Remove-item $destination}
Add-Type -assembly "system.io.compression.filesystem"
[io.compression.zipfile]::CreateFromDirectory($Source, $destination)

