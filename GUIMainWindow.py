# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'GUIMainWindow.ui'
#
# Created by: PyQt5 UI code generator 5.11.2
#
# WARNING! All changes made in this file will be lost!

import matplotlib as mpl
mpl.use('Qt5Agg')

import csv
import itertools
import json
import os
from pathlib import Path
import re
from shutil import copyfile
import sys
import urllib.request

from google_drive_downloader import GoogleDriveDownloader as gdd
import numpy as np
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QRegExp
from PyQt5.QtWidgets import (QColorDialog, QApplication, QWidget, QPushButton,
                               QMessageBox, QFileDialog, QAction, QMainWindow,)
from PyQt5.QtGui import QColor, QBrush, QRegExpValidator

import importData as iD

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(729, 702)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.add_scan = QtWidgets.QPushButton(self.centralwidget)
        self.add_scan.setGeometry(QtCore.QRect(66, 360, 91, 31))
        self.add_scan.setObjectName("add_scan")
        self.label_14 = QtWidgets.QLabel(self.centralwidget)
        self.label_14.setGeometry(QtCore.QRect(336, 20, 35, 16))
        self.label_14.setObjectName("label_14")
        self.lable_ref = QtWidgets.QLabel(self.centralwidget)
        self.lable_ref.setGeometry(QtCore.QRect(386, 520, 52, 16))
        self.lable_ref.setObjectName("lable_ref")
        self.ref_width_2 = QtWidgets.QLabel(self.centralwidget)
        self.ref_width_2.setGeometry(QtCore.QRect(460, 20, 32, 16))
        self.ref_width_2.setObjectName("ref_width_2")
        self.splitter_10 = QtWidgets.QSplitter(self.centralwidget)
        self.splitter_10.setGeometry(QtCore.QRect(36, 620, 121, 24))
        self.splitter_10.setOrientation(QtCore.Qt.Horizontal)
        self.splitter_10.setObjectName("splitter_10")
        self.label_12 = QtWidgets.QLabel(self.splitter_10)
        self.label_12.setObjectName("label_12")
        self.label_x_pos = QtWidgets.QLineEdit(self.splitter_10)
        self.label_x_pos.setObjectName("label_x_pos")
        self.legend = QtWidgets.QCheckBox(self.centralwidget)
        self.legend.setGeometry(QtCore.QRect(129, 430, 58, 17))
        self.legend.setObjectName("legend")
        self.sel_col = QtWidgets.QPushButton(self.centralwidget)
        self.sel_col.setGeometry(QtCore.QRect(426, 360, 91, 31))
        self.sel_col.setObjectName("sel_col")
        self.move_up = QtWidgets.QPushButton(self.centralwidget)
        self.move_up.setGeometry(QtCore.QRect(586, 130, 91, 31))
        self.move_up.setObjectName("move_up")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(36, 480, 41, 16))
        self.label_4.setObjectName("label_4")
        self.label_8 = QtWidgets.QLabel(self.centralwidget)
        self.label_8.setGeometry(QtCore.QRect(456, 480, 31, 16))
        self.label_8.setObjectName("label_8")
        self.dspace = QtWidgets.QCheckBox(self.centralwidget)
        self.dspace.setGeometry(QtCore.QRect(265, 430, 39, 17))
        self.dspace.setObjectName("dspace")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setGeometry(QtCore.QRect(36, 520, 41, 16))
        self.label_5.setObjectName("label_5")
        self.normalise = QtWidgets.QCheckBox(self.centralwidget)
        self.normalise.setGeometry(QtCore.QRect(36, 430, 69, 17))
        self.normalise.setObjectName("normalise")
        self.box = QtWidgets.QCheckBox(self.centralwidget)
        self.box.setGeometry(QtCore.QRect(208, 430, 41, 17))
        self.box.setObjectName("box")
        self.unit_combo = QtWidgets.QComboBox(self.centralwidget)
        self.unit_combo.setGeometry(QtCore.QRect(596, 20, 91, 22))
        self.unit_combo.setObjectName("unit_combo")
        self.unit_combo.addItem("")
        self.unit_combo.addItem("")
        self.unit_combo.addItem("")
        self.unit_combo.addItem("")
        self.label_15 = QtWidgets.QLabel(self.centralwidget)
        self.label_15.setGeometry(QtCore.QRect(246, 560, 58, 16))
        self.label_15.setObjectName("label_15")
        self.add_ref = QtWidgets.QPushButton(self.centralwidget)
        self.add_ref.setGeometry(QtCore.QRect(186, 360, 91, 31))
        self.add_ref.setObjectName("add_ref")
        self.export_options = QtWidgets.QPushButton(self.centralwidget)
        self.export_options.setGeometry(QtCore.QRect(576, 460, 111, 31))
        self.export_options.setObjectName("export_options")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(36, 600, 91, 16))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.splitter_11 = QtWidgets.QSplitter(self.centralwidget)
        self.splitter_11.setGeometry(QtCore.QRect(186, 620, 111, 24))
        self.splitter_11.setOrientation(QtCore.Qt.Horizontal)
        self.splitter_11.setObjectName("splitter_11")
        self.label_13 = QtWidgets.QLabel(self.splitter_11)
        self.label_13.setObjectName("label_13")
        self.label_y_pos = QtWidgets.QLineEdit(self.splitter_11)
        self.label_y_pos.setObjectName("label_y_pos")
        self.bold_label = QtWidgets.QCheckBox(self.centralwidget)
        self.bold_label.setGeometry(QtCore.QRect(415, 560, 53, 17))
        self.bold_label.setObjectName("bold_label")
        self.series_label = QtWidgets.QCheckBox(self.centralwidget)
        self.series_label.setGeometry(QtCore.QRect(389, 430, 80, 17))
        self.series_label.setObjectName("series_label")
        self.export = QtWidgets.QPushButton(self.centralwidget)
        self.export.setGeometry(QtCore.QRect(586, 410, 91, 31))
        self.export.setObjectName("export")
        self.table = QtWidgets.QTableView(self.centralwidget)
        self.table.setGeometry(QtCore.QRect(36, 70, 521, 271))
        self.table.setMouseTracking(True)
        self.table.setObjectName("table")
        self.label_9 = QtWidgets.QLabel(self.centralwidget)
        self.label_9.setGeometry(QtCore.QRect(36, 20, 63, 16))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label_9.setFont(font)
        self.label_9.setObjectName("label_9")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(36, 410, 81, 16))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.plot = QtWidgets.QPushButton(self.centralwidget)
        self.plot.setGeometry(QtCore.QRect(586, 360, 91, 31))
        self.plot.setObjectName("plot")
        self.remove = QtWidgets.QPushButton(self.centralwidget)
        self.remove.setGeometry(QtCore.QRect(306, 360, 91, 31))
        self.remove.setObjectName("remove")
        self.label_10 = QtWidgets.QLabel(self.centralwidget)
        self.label_10.setGeometry(QtCore.QRect(36, 560, 56, 16))
        self.label_10.setObjectName("label_10")
        self.move_down = QtWidgets.QPushButton(self.centralwidget)
        self.move_down.setGeometry(QtCore.QRect(586, 230, 91, 31))
        self.move_down.setObjectName("move_down")
        self.raman = QtWidgets.QCheckBox(self.centralwidget)
        self.raman.setGeometry(QtCore.QRect(496, 430, 56, 17))
        self.raman.setObjectName("raman")
        self.label_11 = QtWidgets.QLabel(self.centralwidget)
        self.label_11.setGeometry(QtCore.QRect(384, 560, 24, 16))
        self.label_11.setObjectName("label_11")
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setGeometry(QtCore.QRect(236, 480, 37, 16))
        self.label_6.setObjectName("label_6")
        self.latex = QtWidgets.QCheckBox(self.centralwidget)
        self.latex.setGeometry(QtCore.QRect(320, 430, 50, 17))
        self.latex.setObjectName("latex")
        self.bold_number = QtWidgets.QCheckBox(self.centralwidget)
        self.bold_number.setGeometry(QtCore.QRect(476, 560, 65, 17))
        self.bold_number.setObjectName("bold_number")
        self.font = QtWidgets.QFontComboBox(self.centralwidget)
        self.font.setGeometry(QtCore.QRect(109, 560, 112, 20))
        self.font.setObjectName("font")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(36, 460, 81, 16))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.label_7 = QtWidgets.QLabel(self.centralwidget)
        self.label_7.setGeometry(QtCore.QRect(236, 520, 58, 16))
        self.label_7.setObjectName("label_7")
        self.x_label = QtWidgets.QLineEdit(self.centralwidget)
        self.x_label.setGeometry(QtCore.QRect(80, 480, 131, 21))
        self.x_label.setObjectName("x_label")
        self.y_label = QtWidgets.QLineEdit(self.centralwidget)
        self.y_label.setGeometry(QtCore.QRect(80, 520, 131, 21))
        self.y_label.setObjectName("y_label")
        self.x_min = QtWidgets.QLineEdit(self.centralwidget)
        self.x_min.setGeometry(QtCore.QRect(280, 480, 61, 21))
        self.x_min.setObjectName("x_min")
        self.x_max = QtWidgets.QLineEdit(self.centralwidget)
        self.x_max.setGeometry(QtCore.QRect(350, 480, 61, 21))
        self.x_max.setObjectName("x_max")
        self.x_inc = QtWidgets.QLineEdit(self.centralwidget)
        self.x_inc.setGeometry(QtCore.QRect(490, 480, 61, 21))
        self.x_inc.setObjectName("x_inc")
        self.scan_width = QtWidgets.QLineEdit(self.centralwidget)
        self.scan_width.setGeometry(QtCore.QRect(300, 520, 61, 21))
        self.scan_width.setObjectName("scan_width")
        self.ref_width = QtWidgets.QLineEdit(self.centralwidget)
        self.ref_width.setGeometry(QtCore.QRect(450, 520, 61, 21))
        self.ref_width.setObjectName("ref_width")
        self.font_size = QtWidgets.QLineEdit(self.centralwidget)
        self.font_size.setGeometry(QtCore.QRect(300, 560, 61, 21))
        self.font_size.setObjectName("font_size")
        self.Name = QtWidgets.QLineEdit(self.centralwidget)
        self.Name.setGeometry(QtCore.QRect(100, 20, 201, 21))
        self.Name.setObjectName("Name")
        self.fig_height = QtWidgets.QLineEdit(self.centralwidget)
        self.fig_height.setGeometry(QtCore.QRect(380, 20, 61, 21))
        self.fig_height.setText("")
        self.fig_height.setObjectName("fig_height")
        self.fig_width = QtWidgets.QLineEdit(self.centralwidget)
        self.fig_width.setGeometry(QtCore.QRect(500, 20, 61, 21))
        self.fig_width.setText("")
        self.fig_width.setObjectName("fig_width")
        self.label_16 = QtWidgets.QLabel(self.centralwidget)
        self.label_16.setGeometry(QtCore.QRect(310, 620, 91, 21))
        self.label_16.setObjectName("label_16")
        self.label_colour = QtWidgets.QCheckBox(self.centralwidget)
        self.label_colour.setGeometry(QtCore.QRect(470, 620, 81, 21))
        self.label_colour.setObjectName("label_colour")
        self.label_font_size = QtWidgets.QLineEdit(self.centralwidget)
        self.label_font_size.setGeometry(QtCore.QRect(390, 620, 56, 24))
        self.label_font_size.setObjectName("label_font_size")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 729, 21))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        self.menuTools = QtWidgets.QMenu(self.menubar)
        self.menuTools.setObjectName("menuTools")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionLoad_Defaults = QtWidgets.QAction(MainWindow)
        self.actionLoad_Defaults.setObjectName("actionLoad_Defaults")
        self.actionSave_Defaults = QtWidgets.QAction(MainWindow)
        self.actionSave_Defaults.setObjectName("actionSave_Defaults")
        self.actionExport_Options = QtWidgets.QAction(MainWindow)
        self.actionExport_Options.setObjectName("actionExport_Options")
        self.actionLoad_Plot = QtWidgets.QAction(MainWindow)
        self.actionLoad_Plot.setObjectName("actionLoad_Plot")
        self.actionSave_Plot = QtWidgets.QAction(MainWindow)
        self.actionSave_Plot.setObjectName("actionSave_Plot")
        self.actionConvert_References = QtWidgets.QAction(MainWindow)
        self.actionConvert_References.setObjectName("actionConvert_References")
        self.actionUpdate_Check = QtWidgets.QAction(MainWindow)
        self.actionUpdate_Check.setObjectName("actionUpdate_Check")
        self.menuTools.addAction(self.actionLoad_Defaults)
        self.menuTools.addAction(self.actionSave_Defaults)
        self.menuTools.addAction(self.actionLoad_Plot)
        self.menuTools.addAction(self.actionSave_Plot)
        self.menuTools.addSeparator()
        self.menuTools.addAction(self.actionExport_Options)
        self.menuTools.addAction(self.actionConvert_References)
        self.menuTools.addAction(self.actionUpdate_Check)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuTools.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.add_scan.setText(_translate("MainWindow", "Add Scan"))
        self.label_14.setText(_translate("MainWindow", "Height:"))
        self.lable_ref.setText(_translate("MainWindow", "Ref Width:"))
        self.ref_width_2.setText(_translate("MainWindow", "Width:"))
        self.label_12.setText(_translate("MainWindow", "X Position:"))
        self.legend.setText(_translate("MainWindow", "Legend"))
        self.sel_col.setText(_translate("MainWindow", "Select Colour"))
        self.move_up.setText(_translate("MainWindow", "Move Up"))
        self.label_4.setText(_translate("MainWindow", "X Label: "))
        self.label_8.setText(_translate("MainWindow", "X Inc:"))
        self.dspace.setText(_translate("MainWindow", "1/d"))
        self.label_5.setText(_translate("MainWindow", "Y Label: "))
        self.normalise.setText(_translate("MainWindow", "Normalise"))
        self.box.setText(_translate("MainWindow", "Box"))
        self.unit_combo.setItemText(0, _translate("MainWindow", "Centimetres"))
        self.unit_combo.setItemText(1, _translate("MainWindow", "Inches"))
        self.unit_combo.setItemText(2, _translate("MainWindow", "Points"))
        self.unit_combo.setItemText(3, _translate("MainWindow", "Pixels"))
        self.label_15.setText(_translate("MainWindow", "Font Size"))
        self.add_ref.setText(_translate("MainWindow", "Add Reference"))
        self.export_options.setText(_translate("MainWindow", "Export Options"))
        self.label_3.setText(_translate("MainWindow", "Label Options:"))
        self.label_13.setText(_translate("MainWindow", "Y Spacing:"))
        self.bold_label.setText(_translate("MainWindow", "Labels"))
        self.series_label.setText(_translate("MainWindow", "Series Label"))
        self.export.setText(_translate("MainWindow", "Export"))
        self.label_9.setText(_translate("MainWindow", "Plot Title:"))
        self.label.setText(_translate("MainWindow", "Plot Options:"))
        self.plot.setText(_translate("MainWindow", "Plot"))
        self.remove.setText(_translate("MainWindow", "Remove"))
        self.label_10.setText(_translate("MainWindow", "Font Name:"))
        self.move_down.setText(_translate("MainWindow", "Move Down"))
        self.raman.setText(_translate("MainWindow", "Raman"))
        self.label_11.setText(_translate("MainWindow", "Bold:"))
        self.label_6.setText(_translate("MainWindow", "X Limit: "))
        self.latex.setText(_translate("MainWindow", "Latex"))
        self.bold_number.setText(_translate("MainWindow", "Numbers"))
        self.label_2.setText(_translate("MainWindow", "Axis Options:"))
        self.label_7.setText(_translate("MainWindow", "Scan Width:"))
        self.x_label.setText(_translate("MainWindow", "2θ (°)"))
        self.y_label.setText(_translate("MainWindow", "Itensity (a.u.)"))
        self.Name.setText(_translate("MainWindow", "Untitled"))
        self.label_16.setText(_translate("MainWindow", "Label Font Size:"))
        self.label_colour.setText(_translate("MainWindow", "Label Colour"))
        self.menuFile.setTitle(_translate("MainWindow", "File"))
        self.menuTools.setTitle(_translate("MainWindow", "Tools"))
        self.actionLoad_Defaults.setText(_translate("MainWindow", "Load Defaults"))
        self.actionSave_Defaults.setText(_translate("MainWindow", "Save Defaults"))
        self.actionExport_Options.setText(_translate("MainWindow", "Export Options"))
        self.actionLoad_Plot.setText(_translate("MainWindow", "Load Plot Data"))
        self.actionSave_Plot.setText(_translate("MainWindow", "Save Plot Data"))
        self.actionConvert_References.setText(_translate("MainWindow", "Convert References"))
        self.actionUpdate_Check.setText(_translate("MainWindow", "Check for Updates"))

        self.model = QtGui.QStandardItemModel()
        self.model.setHorizontalHeaderLabels(['Colour', 'File Name',
                                              'File Path', 'Type',
                                              'Source', 'Y Spacing',
                                              'Y Scale', 'X Shift', 'Marker'])

        fonts = sorted(set([f.name for f in mpl.font_manager.fontManager.ttflist]))
        for r in range(ui.font.count()):
            self.font.removeItem(0)

        for font in fonts:
            self.font.addItem(font)

        self.table.setModel(self.model)
        self.plot_num = 0
        self.lgd = 1
        self.fig = None
        self.defaults = defaults
        self.scan_path = defaults['scan_path']
        self.ref_path = defaults['ref_path']
        self.raman_path = defaults['raman_path']

        self.connections()
        self.set_defaults()

    def connections(self):
        self.table_data = []

        self.add_scan.clicked.connect(self.add_scans)
        self.add_ref.clicked.connect(lambda: self.add_data('Ref',self.ref_path))
        self.remove.clicked.connect(self.remove_rows)
        self.sel_col.clicked.connect(self.select_colour)
        self.move_down.clicked.connect(self.move_rows_down)
        self.move_up.clicked.connect(self.move_rows_up)
        self.plot.clicked.connect(self.plot_data)
        self.export.clicked.connect(self.exports)

        self.actionSave_Defaults.triggered.connect(self.save_defaults)
        self.actionLoad_Defaults.triggered.connect(self.load_defaults)
        self.actionConvert_References.triggered.connect(
            lambda: self.convert_ref('Co'))
        self.actionSave_Plot.triggered.connect(self.save_plot)
        self.actionLoad_Plot.triggered.connect(self.load_plot)
        self.actionUpdate_Check.triggered.connect(self.updates)

    def set_defaults(self):
        textboxes = ['x_label', 'y_label', 'x_min',
        'x_max', 'x_inc', 'scan_width',
        'ref_width', 'font_size', 'fig_height',
        'fig_width', 'label_x_pos', 'label_y_pos', 'label_font_size']

        checkboxes = ['normalise', 'legend', 'dspace',
                'latex', 'raman', 'series_label',
                'box', 'bold_label', 'bold_number']

        self.inputs = textboxes[:]
        self.inputs.extend(checkboxes)
        self.check_or_text = [1]*len(textboxes)
        self.check_or_text.extend([0]*len(checkboxes))

        for ind, text in zip(self.check_or_text, self.inputs):
            if ind == 1:
                getattr(self,text).setText(self.defaults[f'd_{text}'])
            else:
                getattr(self,text).setChecked(float(self.defaults[f'd_{text}']))

        self.colours = {}
        self.colours['Scan'] = [self.rgb2hex(*eval(col)) for col in 
                            self.defaults['d_scan_colours']
                            *int(self.defaults['scan_mult'])]
        self.colours['Ref'] = [self.rgb2hex(*eval(col)) for col in 
                            self.defaults['d_ref_colours']
                            *int(self.defaults['ref_mult'])]
        self.font.setCurrentText(self.defaults['d_font_name'])
        self.unit_combo.setCurrentText(self.defaults['d_unit'])

    def read_defaults(self):
        custom_dict = {f'd_{text}' : getattr(self,text).text()
                        if ind == 1
                        else str(int((getattr(self,text).isChecked())))
                        for ind, text in zip(self.check_or_text, self.inputs)}

        for key in self.defaults.keys():
            if key[2:] not in self.inputs:
                custom_dict[key] = self.defaults[key]

        return custom_dict


    def add_data(self, data_type, path):
        self.open_file_names(data_type, path)
        self.update_table()

        if 'Scan' in data_type:
            data_type = 'Scan'

        if len(self.table_data) == 0:
            tbl_len = 0
        else:
            tbl_len = len(self.table_data[0])

        num = len(self.files_to_add)
        cols_to_add = []

        if len(self.colours[data_type]) >= num:
            for n in range(num):
                cols_to_add.append(self.colours[data_type].pop(0))
        elif len(self.colours[data_type]) < num:
            for n in range(len(self.colours[data_type])):
                cols_to_add.append(self.colours[data_type].pop(0))
            cols_to_add = (cols_to_add + num * ['#000000'])[:num]
        else:
            cols_to_add = (cols_to_add + num * ['#000000'])[:num]


        data_to_add = [
                    cols_to_add, 
                    [x.split('/')[-1].split('.')[0] for x in self.files_to_add],
                    self.files_to_add,
                    [data_type]*num,
                    ['Mo']*num,
                    [self.defaults['d_y_spacing']]*num,
                    [self.defaults['d_y_scale']]*num,
                    [self.defaults['d_x_shift']]*num,
                    ]
        self.table_data.append(data_to_add)
        self.table_data_update(data_to_add, tbl_len, num)

    def table_data_update(self, data_to_add, tbl_len, num):
        data_to_add = zip(*data_to_add)

        for data in data_to_add:
            row = []
            for n, element in enumerate(data):
                self.item = QtGui.QStandardItem(element)
                if n == 2 or n == 3:
                    self.item.setEditable(False)

                row.append(self.item)
            self.model.appendRow(row)

        # Create new combobox
        self.source_list = ['Mo', 'Co', 'Fe', 'Cr', 'Cu']
        self.source_index = {source : i for i, 
                            source in enumerate(self.source_list)}

        for row in range(tbl_len,tbl_len + num):
            c = QtWidgets.QComboBox()
            c.addItems(self.source_list)
            c.setCurrentText(self.defaults['d_source'])
            i = self.table.model().index(row,4)
            self.table.setIndexWidget(i,c)

            cell = self.table.model().item(row,0)
            col = QBrush(QColor(cell.text()))
            cell.setBackground(col)


    def close_application(self):
        buttonReply = QMessageBox.question(
                                MainWindow, 'Update message',
                                "Are you sure you would like to quit?",
                                QMessageBox.Yes | QMessageBox.No,
                                QMessageBox.No)
        if buttonReply == QMessageBox.Yes:
            MainWindow.close()
        else:
            print('No clicked.')


    def open_file_names(self, data_type, path, add=True):
        if path == '':
            path = file_path
            directory = str(path / (data_type + 's'))
        else:
            directory = path

        data_format = {
                       'Scan' : 'XY Files (*.xy)',
                       'Ref' : 'TXT Files (*.txt)',
                       'Raman' : 'TXT Files (*.txt)',
                       'Defaults' : 'cfg Files (*.cfg)',
                       'Plot' : 'plt Files (*.plt)',
                       }
        
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        files, _ = QFileDialog.getOpenFileNames(
                                MainWindow,"Add XRD data", str(directory),
                                data_format[data_type.split(' ')[0]]
                                + ";;All files (*)",
                                options=options)

        if files and add is True:
            self.files_to_add = files
        else:
            self.files_to_add = []
            return files
        
        self.update_table()

    def add_scans(self):
        if self.raman.isChecked():
            self.add_data('Raman Scan',self.raman_path)
        else:
            self.add_data('Scan',self.scan_path)

    def update_table(self):
        model = self.table.model()
        data = []
        for row in range(model.rowCount()):
          data.append([])
          for column in range(model.columnCount()):
            index = model.index(row, column)
            data[row].append(str(model.data(index)))
        
        data = list(zip(*data))
        self.table_data = data

    def update_table_combobox(self):
        model = self.table.model()
        data = []
        for row in range(model.rowCount()):
          data.append([])
          for column in range(model.columnCount()):
            index = model.index(row, column)
            if column == 4:
                data[row].append(self.table.indexWidget(index).currentText())
            else:
                data[row].append(str(model.data(index)))

        data = list(zip(*data))
        self.table_data = data

    def remove_rows(self):
        selected_rows = self.unique_rows()

        for row in selected_rows:
            selected_rows = self.unique_rows()
            self.table.model().removeRow(selected_rows[0])

            self.update_table()

    def select_colour(self):
        selected_rows = self.unique_rows()

        colour = QColorDialog.getColor()

        for row in selected_rows:
            index = self.table.currentIndex().sibling(row,0)
            self.table.model().setData(index,colour.name())

            col = QBrush(colour)
            self.table.model().item(row,0).setBackground(col)
        
        self.update_table()

    def rgb2hex(self,r,g,b):
        return f'#{int(round(r)):02x}{int(round(g)):02x}{int(round(b)):02x}'

    def unique_rows(self):
        selected_rows = [index.row() for index in self.table.selectedIndexes()]
        selected_rows = list(set(selected_rows))
        return selected_rows

    def move_rows_down(self):
        self.update_table_combobox()

        if [] in self.table_data or self.table_data == []:
            print('No data selected')  
        else:
            selected_rows = self.unique_rows()
            combo_selection = list(self.table_data[4])
            
            if len(selected_rows) > 0:
                if selected_rows[-1] == self.table.model().rowCount()-1:
                    print("Can't move last row down")
                else:
                    new_ind = self.shift_selection(1)

                    for row in selected_rows:
                        row_move = self.table.model().takeRow(selected_rows[0])
                        self.table.model().insertRow(
                                                selected_rows[-1]+1, row_move)
                        combo_move = combo_selection.pop(selected_rows[0])
                        combo_selection.insert(selected_rows[-1]+1, combo_move)
                        
                    self.change_selection(new_ind)
                    self.add_combo_box(combo_selection)
                    self.update_table_combobox()     

    def move_rows_up(self):
        self.update_table_combobox()

        if [] in self.table_data or self.table_data == []:
            print('No data selected')  
        else:
            selected_rows = self.unique_rows()
            combo_selection = list(self.table_data[4])
            
            if len(selected_rows) > 0:
                if selected_rows[0] == 0:
                    print("Can't move first row up")
                else:
                    new_ind = self.shift_selection(-1)

                    for row in selected_rows:
                        row_move = self.table.model().takeRow(row)
                        self.table.model().insertRow(row-1,row_move)
                        combo_move = combo_selection.pop(row)
                        combo_selection.insert(row-1,combo_move)

                    self.change_selection(new_ind)
                    self.add_combo_box(combo_selection)
                    self.update_table_combobox()

    def move_rows_down_mac(self):
        selected_rows = self.unique_rows()
        
        if len(selected_rows) > 0:
            if selected_rows[-1] == self.table.model().rowCount()-1:
                print("Can't move last row down")
            else:
                new_ind = self.shift_selection(1)

                for row in selected_rows:
                    row_move = self.table.model().takeRow(selected_rows[0])
                    self.table.model().insertRow(selected_rows[-1]+1,row_move)

                self.change_selection(new_ind)
                self.update_table()

    def move_rows_up_mac(self):
        selected_rows = self.unique_rows()

        if len(selected_rows) > 0:
            if selected_rows[0] == 0:
                print("Can't move first row up")
            else:
                new_ind = self.shift_selection(-1)

                for row in selected_rows:
                    row_move = self.table.model().takeRow(row)
                    self.table.model().insertRow(row-1,row_move)

                self.change_selection(new_ind)
                self.update_table()

    def add_combo_box(self,combo_selection):
  
        for row, combo_text in enumerate(combo_selection):
            c = QtWidgets.QComboBox()
            c.addItems(self.source_list)
            i = self.table.model().index(row,4)
            self.table.setIndexWidget(i,c)
            combo_index = self.source_index[combo_text]
            self.table.indexWidget(i).setCurrentIndex(combo_index)

    def shift_selection(self,row_shift,col_shift=0):
        indexes = self.table.selectedIndexes()
        new_indexes = []

        for index in indexes:
            row = index.row() + row_shift
            col = index.column() + col_shift

            new_indexes.append(indexes[0].sibling(row,col))

        return new_indexes

    def convert_ref(self, file_path, source='Co'):
        file_path = self.open_file_names('Ref', self.ref_path, add=False)

        for paths in file_path:
            print('Converting: paths\n')
            theta, intensity = iD.importScans(paths, data_type='Ref')
            dspa = 1/iD.dspace_conv(theta, source)
            path = Path(paths)
            new_path = path.parent / 'converted'
            if new_path.exists() == False:
                new_path.mkdir()

            new_path = new_path / path.name

            with open(new_path, 'w+') as file:
                file.write('dspa(A)\tInt\n')
                for dspa_row, int_row in zip(dspa, intensity):
                    file.write('{:.6}\t{}\n'.format(dspa_row, int_row))


    def change_selection(self,new_indexes):
        selItem = QtCore.QItemSelection(new_indexes[0],new_indexes[-1])
        selFlags = QtCore.QItemSelectionModel().SelectionFlags(3)

        self.table.selectionModel().select(selItem,selFlags)


    def save_defaults(self):
        custom_defaults = self.read_defaults()
        name = QFileDialog.getSaveFileName(QWidget(),'Save File',
                    'custom_defaults.cfg',"Config File (*.cfg);;All Files (*)")

        if name[0] != '':
            with open(name[0],'w') as file:
                json.dump(custom_defaults, file, indent=4)


    def load_defaults(self):
        self.open_file_names('Defaults', file_path)
        if self.files_to_add != []:
            with open(self.files_to_add[0]) as f:
                defaults = json.load(f)

            self.defaults = defaults
            self.set_defaults()
        
    def save_plot(self):
        self.update_table_combobox()

        custom_plot = {}
        custom_plot['defaults'] = self.read_defaults()
        custom_plot['table_data'] = self.table_data

        name = QFileDialog.getSaveFileName(QWidget(),'Save File',
                    'Plot Data/plot_data.plt',
                    'Plot data file (*.plt);;All Files (*)')

        if name[0] != '':
            with open(name[0],'w') as file:
                json.dump(custom_plot, file, indent=4)

    def load_plot(self):
        self.table.model().clear()
        self.open_file_names('Plot', file_path / 'Plot Data')

        if self.files_to_add != []:
            with open(self.files_to_add[0]) as f:
                custom_plot = json.load(f)

            paths = custom_plot['table_data'][2]
            if any([Path(path).exists() for path in paths]) == False:
                print('The following paths do not exist:')
                print('\n'.join(['\t'+path for path in paths]))
            else:
                self.table_data = custom_plot['table_data']
                self.defaults = custom_plot['defaults']
                self.set_defaults()
                self.table_data = custom_plot['table_data']

                tbl_len = len(self.table_data[0])
                self.table_data_update(self.table_data, 0, tbl_len)


    def exports(self):
        if self.fig is not None:
            plotname = self.Name.text()
            if self.lgd == 1:
                self.fig.savefig('Plots/' + plotname+'.svg')
            else:
                self.fig.savefig(plotname+'.svg', 
                            bbox_extra_artists=(self.lgd,), bbox_inches='tight')

    def plot_data(self):
        self.update_table()    
        if [] in self.table_data or self.table_data == []:
            print('No data selected')  
        else:
            iD.plotting(self, file_path)

    def updates(self):
        def mycmp(version1, version2):
            def normalize(v):
                return [int(x) for x in re.sub(r'(\.0+)*$','', v).split(".")]
            def cmp(a,b):
                return (a > b) - (a < b)
            return cmp(normalize(version1), normalize(version2))

        def version_message(test):
            msgBox = QMessageBox()
            if test > 0:
                buttonReply = QMessageBox.question(
                                MainWindow, 'Update message',
                                ("An update is available. Would you like to "
                                "download the update?"),
                                QMessageBox.Yes | QMessageBox.No,
                                QMessageBox.No)
                if buttonReply == QMessageBox.Yes:
                    info = QMessageBox.information(
                                MainWindow, 'Update message',
                                'Downloading updates ~100MB\nRestart application when download is complete')
            else:
                info = QMessageBox.information(
                                MainWindow, 'Update message',
                                "Your version is up to date",
                                QMessageBox.Ok,
                                QMessageBox.Ok)

        try:
            changelog_link = 'https://gitlab.com/dominic.deo8/' \
                             'XRDPlot/raw/master/CHANGELOG'
            with urllib.request.urlopen(changelog_link) as response:
                changelog = response.read().decode()
                latest_version = changelog.split('\n')[0].split(' ')[1]
        except:
            print('Cannot connect to internet to check for updates')

        with open('CHANGELOG') as log:
            current_version = log.readline().split(' ')[1]
            test = mycmp(latest_version, current_version)
            version_message(test)
 


##################################################################
#### Change directories
##################################################################

if getattr(sys, 'frozen', False):
    file_path = Path(sys.executable).resolve().parent
elif __file__:
    file_path = Path(__file__).resolve().parent

os.chdir(file_path)
source = file_path / 'Styles' / 'thesis.mplstyle'
dest = Path(mpl.get_configdir()) / 'stylelib' / 'thesis.mplstyle'

if dest.parent.exists() == False:
    dest.parent.mkdir()

copyfile(source,dest)
import matplotlib.pyplot as plt


with open('defaults.cfg') as f:
    defaults = json.load(f)

### Check path validity

def path_check(key):
    if Path(defaults[key]).is_dir() == False:
        print(f'{key} is not a valid directory, using default path instead')
        defaults[key] = ''   

for paths in ['ref', 'scan', 'raman']:
    path_check(paths + '_path')

##################################################################
####### Main Loop 
##################################################################

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())



##### 
## Add check for paths and for paths in plot to see if they still exist and then to edit the pathwith browse to new path
