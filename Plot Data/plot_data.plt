{
    "defaults": {
        "d_x_label": "2\u03b8 (\u00b0)",
        "d_y_label": "Intensity (a.u.)",
        "d_x_min": "20",
        "d_x_max": "120",
        "d_x_inc": "10",
        "d_scan_width": "0.5",
        "d_ref_width": "2",
        "d_font_size": "11",
        "d_fig_height": "6.5",
        "d_fig_width": "12",
        "d_label_x_pos": "80",
        "d_label_y_pos": "0.1",
        "d_normalise": "1",
        "d_legend": "0",
        "d_dspace": "0",
        "d_latex": "0",
        "d_raman": "0",
        "d_series_label": "1",
        "d_box": "1",
        "d_bold_label": "1",
        "d_bold_number": "0",
        "d_scan_colours": [
            "(0, 0, 0)"
        ],
        "d_ref_colours": [
            "(199, 65, 49)",
            "(199, 65, 49)"
        ],
        "scan_mult": "10",
        "ref_mult": "1",
        "scan_path": "",
        "ref_path": "",
        "raman_path": "",
        "d_tick_dir": "in",
        "d_font_name": "Arial",
        "d_unit": "Centimetres",
        "d_source": "Co",
        "d_y_spacing": "0.1",
        "d_y_scale": "1",
        "d_x_shift": "0",
        "d_image_format": "SVG",
        "d_image_res": "300",
        "d_top_space": "1.1"
    },
    "table_data": []
}